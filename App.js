/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';

import {FlipCard} from './app/components';

const dummyCards = [
  {
    front: '?',
    back: 45,
  },
  {
    front: '?',
    back: 45,
  },
  {
    front: '?',
    back: 63,
  },
  {
    front: '?',
    back: 63,
  },
  {
    front: '?',
    back: 12,
  },
  {
    front: '?',
    back: 12,
  },
  {
    front: '?',
    back: 92,
  },
  {
    front: '?',
    back: 92,
  },
  {
    front: '?',
    back: 51,
  },
  {
    front: '?',
    back: 51,
  },
  {
    front: '?',
    back: 85,
  },
  {
    front: '?',
    back: 85,
  },
];

const App = () => {
  const [steps, setSteps] = useState(0);
  const [cards, setCards] = useState([]);
  const [isFlip, setIsFlip] = useState(true);
  const [flippedCount, setFlippedCount] = useState(0);
  const isDarkMode = false;

  const onPressFlipCard = () => {
    setSteps(steps + 1);
    setIsFlip(!isFlip);
    console.log('Pressed!');
    setFlippedCount(flippedCount + 1);
  };

  const onPressRestart = () => {
    console.log('Restarted!');
  };

  useEffect(() => {
    setCards(dummyCards);
  }, []);

  useEffect(() => {
    console.log('@@@@@:', flippedCount);
    if (flippedCount === 2) {
      setIsFlip(true);
      setFlippedCount(0);
    }
  }, [flippedCount]);

  return (
    <SafeAreaView>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View style={styles.header}>
        <TouchableWithoutFeedback
          onPress={onPressRestart}
          style={styles.headerItems}>
          <Text style={styles.headerText}> Restart</Text>
        </TouchableWithoutFeedback>
        <View style={[styles.headerItems]}>
          <Text style={[styles.headerText, styles.steps]}>STEPS: {steps}</Text>
        </View>
      </View>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.backgroundStyle}>
        <View style={styles.gameBoard}>
          {cards.map((item, i) => {
            return (
              <FlipCard
                key={i}
                frontStyles={[styles.flipCard]}
                backStyles={[styles.flipCard]}
                onPress={onPressFlipCard}
                frontChildren={
                  <Text style={styles.flipCardFrontText}>{item?.front}</Text>
                }
                backChildren={
                  <Text style={styles.flipCardBackText}>{item?.back}</Text>
                }
                isFlipHorizontal={true}
                isFlip={isFlip}
              />
            );
          })}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  steps: {
    fontSize: 28,
  },
  header: {
    width: 'auto',
    minWidth: 50,
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'dimgray',
    alignItems: 'center',
    paddingLeft: 20,
  },
  headerItems: {
    margin: 15,
  },
  headerText: {
    fontSize: 16,
    color: 'white',
  },
  gameBoard: {
    justifyContent: 'center',
    margin: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingLeft: 0,
  },
  backgroundStyle: {
    backgroundColor: 'dimgray',
    height: '100%',
  },
  flipCard: {
    height: 175,
    minWidth: '30%',
    margin: 2,
    backgroundColor: 'powderblue',
    alignItems: 'center',
    justifyContent: 'center',
  },

  flipCardFrontText: {
    fontSize: 30,
  },
  flipCardBackText: {
    fontSize: 30,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
