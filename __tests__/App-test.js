/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../App';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  renderer.create(<App />);
});

//TODO: Test
//1. Render elements from the array
//2. Update steps on the header
//3. Restart rerenders and reset all
//4. FlipCard front and back values are correct or not
