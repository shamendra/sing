import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Animated,
} from 'react-native';
import PropTypes from 'prop-types';

const FlipCard = ({
  frontChildren,
  backChildren,
  frontStyles,
  backStyles,
  isFlipHorizontal,
  isFlip,
  onPress,
}) => {
  const [animatedValue] = useState(new Animated.Value(0));
  const [value, setValue] = useState(0);

  const frontInterpolate = animatedValue.interpolate({
    inputRange: [0, 180],
    outputRange: ['0deg', '180deg'],
  });
  const backInterpolate = animatedValue.interpolate({
    inputRange: [0, 180],
    outputRange: ['180deg', '360deg'],
  });

  const frontAnimatedStyle = {
    transform: [
      isFlipHorizontal
        ? {rotateY: frontInterpolate}
        : {rotateX: frontInterpolate},
    ],
  };

  const backAnimatedStyle = {
    transform: [
      isFlipHorizontal
        ? {rotateY: backInterpolate}
        : {rotateX: backInterpolate},
    ],
  };

  const flipCard = () => {
    onPress();
    if (isFlip) {
      Animated.spring(animatedValue, {
        toValue: 0,
        friction: 8,
        tension: 10,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.spring(animatedValue, {
        toValue: 180,
        friction: 8,
        tension: 10,
        useNativeDriver: true,
      }).start();
    }
    animatedValue.addListener(({val}) => {
      setValue(val);
    });
  };

  return (
    <TouchableWithoutFeedback onPress={flipCard}>
      <View>
        <Animated.View
          style={[frontStyles, styles.flipCard, frontAnimatedStyle]}>
          {frontChildren}
        </Animated.View>
        <Animated.View
          style={[
            backStyles,
            styles.flipCard,
            styles.flipCardBack,
            backAnimatedStyle,
          ]}>
          {backChildren}
        </Animated.View>
      </View>
    </TouchableWithoutFeedback>
  );
};

FlipCard.propTypes = {
  isFlip: PropTypes.bool,
  isFlipHorizontal: PropTypes.bool,
  style: PropTypes.object,
};

FlipCard.defaultProps = {
  isFlip: false,
  isFlipHorizontal: false,
  styles: {},
};

const styles = StyleSheet.create({
  flipCard: {
    borderColor: 'white',
    borderWidth: 5,
    borderRadius: 20,
    backgroundColor: 'dodgerblue',
    backfaceVisibility: 'hidden',
    height: 175,
    minWidth: '30%',
    margin: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  flipCardBack: {
    width: '96%',
    position: 'absolute',
    top: 0,
    backgroundColor: 'white',
  },
});

export default FlipCard;
